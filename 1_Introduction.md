
## Linux Basic
Linux File system:

![](img/fs.png)

[other docs](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard);
[source](https://www.blackmoreops.com/2015/06/18/linux-file-system-hierarchy-v2-0/)

### Basic command
In linux there's 2 type of paths :
- Absolute (path starting from the beginning (root))
  - Starting alway from `/`
  - Each folder is separated by another `/`
  - Example: /home/user/file
- Relative (path starting from here)
  - `.` means here (if you place nothing in front of a name/folder's name means ./ <- current folder folder)
  - `..` means back
  - `~` means user's home
  - Example: `cd exams`:  means start from here (current folder, obtainable get from command `pwd`), and open folder named exams 
  - Example: `~/file` (the file placed here, in this Working Directory) (Different base on where you are!!)
---
Hint: `tab` is very useful in linux

```bash
# First of all: manuals 
man command # Give you information about that specific command, and HOW TO USE IT
# Print Working Directory 
pwd
# List
ls
ls -l
ls -a
# Change Directory
cd path 
cd .
cd ..
# Create an empty file, or change last-edit time
tocuh filename
# Copy
cp file_to_copy file_of_dest
# Move (also used to rename)
mv file_to_move file_of_dest
# Example if i do: mv ./prova ./prova2
# i'm just renaming the file

# Remove
rm filename
# Inspect file (ASCII)
cat
# Edit a file
nano | vi | vim
```
#### Exercise
1. Try to see man of each of previous commands (try also `man man` )

### Permissions
```bash
# Get your username
whoami
# Obtain info about your userid groupid ..
id
# Obtain your groups
groups
# obtain date
date
# Obtain hos's name
```
![](img/permission.png)

In order to change permissions we need to use chmod command (see the man or this [link](https://it.wikipedia.org/wiki/Chmod))

## Access to MCIMONE-LOGIN
```bash 
ssh username@beta.dei.unibo.it -p 2223 
```
username has to be added registered ( fill this [form](https://forms.gle/huXeBrNapgA9oVFM6) )

#### Exercise
1. Print the current working directory
3. Print user list (/home contain all user's homes)
4. Create file called myfile
4. Rename your file in hello_world.c
5. Make a directory called lab_of_big_data and move your file inside
7. Edit your file and create a real hello-world.c file.

#### More Exercises
1. Create a file with name `my_first_script.sh`
2. Add the x bit for all users (owner, groups, and other)
3. Try also to do it with numeric mode (777-000).
4. Edit your `my_first_script.sh` and insert the right [shabang](https://it.wikipedia.org/wiki/Shabang) inside 
5. Insert inside this command :`echo "There's  $(ls -l | wc -l ) file(s) inside this folder"`
5. Go to see man of each previous command (echo, wc)
6. Try to run your script (figure out how to do)
